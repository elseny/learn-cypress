describe("Walk through a product in itemku.com", function () {
  function getRandomInt(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

  function getTextFromEl($el: JQuery): string {
    return $el.text();
  }

  function convertToInt(text: string): number {
    return +text.substring(3, text.length).split(".").join("");
  }

  it("Full Walkthrough", function () {
    cy.visit("/");

    let gameText: string;
    let selectedGame: number;

    cy.get(`[data-order=${getRandomInt(6)}]`)
      .trigger("mouseover")
      .find(".container-default")
      .children()
      .as("selectedCategory")
      .then(($children) => {
        selectedGame = getRandomInt($children.length);
      });

    cy.get("@selectedCategory")
      .then(($children) => {
        cy.wrap($children).eq(selectedGame);
      })
      .as("selectedGame")
      .find("p")
      .then(($p) => {
        gameText = $p.text().trim();
      });

    cy.get("@selectedGame").click();

    cy.title().should(($title) => {
      expect($title).to.contains(gameText);
    });

    cy.get("#product_table")
      .children()
      .then(($childContent) => {
        cy.wrap($childContent[1].id === "product grid");
      })
      .then(($haveProduct) => {
        if ($haveProduct) {
          cy.get(".new-container-pl")
            .children()
            .then(($products) => {
              cy.wrap($products).eq(getRandomInt($products.length));
            })
            .click();

          cy.get("#title_text_area > .text-medium > :nth-child(1)")
            .invoke("text")
            .should("contains", gameText);

          let pricePerUnit: number, qty: number;

          cy.get("#order_form .text-red.text-bold.text-very-big").then(($p) => {
            pricePerUnit = convertToInt(getTextFromEl($p));
          });

          cy.get("#order_quantity").then(($qty) => {
            qty = +getTextFromEl($qty);
          });

          cy.get("#addBtn").click();

          cy.get("#order_quantity").then(($qty) => {
            qty = +$qty.val()!;
          });

          cy.get("#order_form .text-orange.text-bold.text-big")
            .as("totalPrice")
            .should(($text) => {
              expect(convertToInt(getTextFromEl($text))).to.eq(
                pricePerUnit * qty
              );
            });

          cy.get("#minusBtn").click();

          cy.get("#order_quantity").then(($qty) => {
            qty = +$qty.val()!;
          });

          cy.get("@totalPrice").should(($text) => {
            expect(convertToInt(getTextFromEl($text))).to.eq(
              pricePerUnit * qty
            );
          });

          let randomQty = getRandomInt(99);

          cy.get("#order_quantity")
            .clear()
            .type("" + randomQty);

          cy.get("#order_quantity").then(($qty) => {
            qty = +$qty.val()!;
          });

          cy.get("@totalPrice").should(($text) => {
            expect(convertToInt(getTextFromEl($text))).to.eq(
              pricePerUnit * qty
            );
          });
        }
      });
  });
});

// describe('Test error',()=>{

//   it('test error',function(){
//     cy.visit('https://itemku.com/games/valorant')
//   })
// })
